
// bài 1


function soNguyenNhoNhat(idClick,idResual){
    let click = document.querySelector(idClick);
    let resual = document.querySelector(idResual);

    click.addEventListener("click",function(){
        let sum = 0;
        let soNhoNhat;
        for(let i = 0; i<1e+4; i++){
            sum +=i;
            console.log(sum)
            if(sum>1e+4){
                soNhoNhat = i
                console.log(soNhoNhat)
                resual.innerHTML = `<span>Số nguyên dương nhỏ nhất là: ${soNhoNhat}</span>`
                break;
            }
        }
    })
}

soNguyenNhoNhat(".click1",".resua1 span")


// bài 2

function tinhTong(idSoX,idSoN,idClick,idResual){
    let click = document.querySelector(idClick);

    click.addEventListener("click",function(){
        let soX= document.querySelector(idSoX).value*1;
        let soN = document.querySelector(idSoN).value*1;
        let resual = document.querySelector(idResual)
        let tong = 0;
        for(let n = 1 ; n<=soN ;n++ ){
            let mu = soX**n
            console.log(mu)
            tong +=mu
        }
        resual.innerHTML = `<span>Tổng: ${tong}</span>`
    })
}

tinhTong(".soX",".soN",".click2",".resua2 span")

// Bài 3
function tinhGiaiThua(idNhapSoN, idClick, idResual) {
    let click = document.querySelector(idClick)
    click.addEventListener("click",function() {
        let soN = document.querySelector(idNhapSoN).value*1;
        let resual = document.querySelector(idResual)
        let tinhGiaiThua= 1;
        for(let i = 1; i<=soN; i++){
            tinhGiaiThua = tinhGiaiThua * i;
            resual.innerHTML = `<span>Giai thừa: ${tinhGiaiThua}</span>`
        }
    })
}

tinhGiaiThua(".nhapSoN", ".click3",".resua3 span")


// bài 4

function taoTheDiv(idClick,idResual){
    let click = document.querySelector(idClick)
    click.addEventListener("click",function(){
        let resual = document.querySelector(idResual)
        let container="";
        for(let i = 1 ; i<=10; i++){
            if(i % 2 !== 0){
                container = container + `<div style="background-color: #0D6EFD; color: white" class="py-2 px-3 m-1 rounded" >Div lẽ ${i}</div> `
            }else{
                container = container + `<div style="background-color: #DC3545; color: white" class="py-2 px-3 m-1 rounded" >Div Chẵn ${i}</div>`
            }
            resual.innerHTML = container
        }
    })
}

taoTheDiv(".click4",".resua4 span")

// bài 5:

function inSoNguyenTo(idSo,idClick,idResual){
    let click = document.querySelector(idClick)

    click.addEventListener("click",function(){
        let so = document.querySelector(idSo).value*1
        let resual = document.querySelector(idResual)
        let t = "";
        for(let i = 0; i<= so; i++){
            var flag = true;
            if(i < 2){
                flag = false;
            }else{
                for(let x = 2; x <= Math.sqrt(i); x++){
                    if(i % x == 0){
                        flag = false;
                    }
                }
            }
            if(flag == true){
                console.log(i)
                t = t+ i + " ";
                resual.innerHTML= `Số nguyên tố là: ${t}`
            }
        }
        
    })
}

inSoNguyenTo(".nhapVaoSoN",".click5",".resua5 span")